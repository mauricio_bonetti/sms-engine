package com.bon.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import com.bon.persistence.CustomerDAO;
import com.bon.persistence.SMSDAO;
import com.bon.vo.SMSVO;

public final class SMSService {
	public static void dispatch() {
		List<SMSVO> smssToBeSent = SMSDAO.findAllToBeSent();
		System.out.println("Antes de enviar");
		System.out.println(smssToBeSent);
		scheduleDispatch(smssToBeSent);
		SMSDAO.updateAsSent(smssToBeSent);
		System.out.println("Depois de enviado");
	}

	private static void scheduleDispatch(List<SMSVO> smssToBeSent) {
		try {
			StringBuilder f = new StringBuilder();
			f.append("username=");
			f.append(URLEncoder.encode("mauricio_bonetti", "ISO-8859-1"));
			f.append("&password=");
			f.append(URLEncoder.encode("mlb310586", "ISO-8859-1"));
			URL url = new URL("http://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");
			for (SMSVO vo : smssToBeSent) {
				StringBuilder query = new StringBuilder();
				query.append(f);
				query.append("&message=");
				query.append(URLEncoder.encode(vo.getMessage(), "ISO-8859-1"));
				query.append("&msisdn=");
				query.append("55");
				query.append(CustomerDAO.findMSISDN(vo.getCustomerObjectId()));
				
				System.out.println(query);
				
				URLConnection conn = url.openConnection();
	            conn.setDoOutput(true);
	            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	            wr.write(query.toString());
	            wr.flush();
	            	        
	            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String line;
	            while ((line = rd.readLine()) != null) {
	                System.out.println(line);
	            }
	            wr.close();
	            rd.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
