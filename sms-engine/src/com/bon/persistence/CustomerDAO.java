package com.bon.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class CustomerDAO {
	public static String findMSISDN(long objectId) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = JDBCDAO.getConnection();
		try {
			stmt = conn.prepareStatement("SELECT CUSTOMER_MSISDN FROM SMS_CUSTOMER WHERE CUSTOMER_ID = ?");
			stmt.setLong(1, objectId);
			String msisdn = null;
			rs = stmt.executeQuery();
			if (rs.next()) {
				msisdn = rs.getString("CUSTOMER_MSISDN");
			}
			stmt.close();
			rs.close();
			conn.close();
			return msisdn;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}		
	}
}
