package com.bon.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public abstract class JDBCDAO {
	private static String url = "jdbc:postgresql://localhost/sms-admin?user=postgres&password=";
	
	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}		
	}
	
	public static Connection getConnection() {
		try {
			Connection conn = DriverManager.getConnection(url);
			conn.setAutoCommit(false);
			return conn;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
