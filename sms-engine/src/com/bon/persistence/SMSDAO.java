package com.bon.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bon.vo.SMSVO;

public final class SMSDAO {
	public static List<SMSVO> findAllToBeSent() {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Connection conn = JDBCDAO.getConnection();
		try {
			stmt = conn.prepareStatement("SELECT * FROM SMS_SMS WHERE SMS_SENT = FALSE AND SMS_SCHEDULED_TS <= ?");
			stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
			List<SMSVO> list = new ArrayList<SMSVO>();
			rs = stmt.executeQuery();
			while (rs.next()) {
				SMSVO vo = new SMSVO();
				vo.setObjectId(rs.getLong("SMS_ID"));
				vo.setUserObjectId(rs.getLong("SMS_USER_ID"));
				vo.setCustomerObjectId(rs.getLong("SMS_CUSTOMER_ID"));
				vo.setCreationTimestamp(rs.getTimestamp("SMS_CREATION_TS").getTime());
				vo.setScheduledTimestamp(rs.getTimestamp("SMS_SCHEDULED_TS").getTime());
				vo.setMessage(rs.getString("SMS_MESSAGE"));
				vo.setSent(false);
				list.add(vo);
			}
			stmt.close();
			rs.close();
			conn.close();
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static void updateAsSent(List<SMSVO> list) {
		PreparedStatement stmt = null;
		Connection conn = JDBCDAO.getConnection();
		try {
			for (SMSVO vo : list) {
				stmt = conn.prepareStatement("UPDATE SMS_SMS SET SMS_SENT = TRUE WHERE SMS_ID = ?");
				stmt.setLong(1, vo.getObjectId());
				int r = stmt.executeUpdate();
				if (r != 1) {
					conn.rollback();
					stmt.close();
					conn.close();
					throw new RuntimeException("The update performed must change just one line. Changed " + r + " lines");
				}
				vo.setSent(true);
				stmt.close();				
			}
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}

}
