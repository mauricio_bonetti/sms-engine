package com.bon.vo;


public class SMSVO extends ObjectIdVO {
	private long userObjectId;
	private long customerObjectId;
	private long creationTimestamp;
	private long scheduledTimestamp;
	private String message;
	private boolean sent;
	
	public long getUserObjectId() {
		return userObjectId;
	}
	
	public void setUserObjectId(long userObjectId) {
		this.userObjectId = userObjectId;
	}
	
	public long getCustomerObjectId() {
		return customerObjectId;
	}
	
	public void setCustomerObjectId(long customerObjectId) {
		this.customerObjectId = customerObjectId;
	}
	
	public long getCreationTimestamp() {
		return creationTimestamp;
	}
	
	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	
	public long getScheduledTimestamp() {
		return scheduledTimestamp;
	}
	
	public void setScheduledTimestamp(long scheduledTimestamp) {
		this.scheduledTimestamp = scheduledTimestamp;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public boolean isSent() {
		return sent;
	}
	
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("SMS - ");
		s.append(getObjectId());
		s.append(" - from:");
		s.append(getUserObjectId());
		s.append(" - to:");
		s.append(getCustomerObjectId());
		s.append(" - message:");
		s.append(getMessage());		
		s.append(" - sent:");
		s.append(isSent());
		return s.toString();
	}
}
